#!/bin/bash

#####################################
# All the include script will be here
#####################################

. includes/core.sh

function GetInstaller {

	clear
	echo -ne "\n\n\n"
	echo -ne "############################\n"
	colorMsg "PART 21: Get Panel Installer" green
	echo -ne "############################"
	echo -ne "\n\n\n"
	sleep 1

	curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin/
	cd /home/whop/panel/

	git clone $3
	cd whop-panel-installer
	git reset --hard $4

	cd /home/whop/panel/
	chown -R $2:$2 whop-panel-installer

	cd $1
}

function GetInstallerNode {

	clear
	echo -ne "\n\n\n"
	echo -ne "####################################\n"
	colorMsg "PART 22: Get Panel Installer Backend" green
	echo -ne "####################################"
	echo -ne "\n\n\n"
	sleep 1

	cd /home/whop/panel/
	git clone $2

	cd /home/whop/panel/whop-node-installer
	git reset --hard $3
	npm install

	cd $1
}


function GetNode {

	clear
	echo -ne "\n\n\n"
	echo -ne "#######################\n"
	colorMsg "PART 23: Get Panel Node" green
	echo -ne "#######################"
	echo -ne "\n\n\n"
	sleep 1


	cd /home/whop/panel/
	git clone $2

	cd /home/whop/panel/whop-node
	git reset --hard $3
	npm install


	cd $1
}

function GetPanel {

	clear
	echo -ne "\n\n\n"
	echo -ne "#######################\n"
	colorMsg "PART 24: Get WHoP Panel" green
	echo -ne "#######################"
	echo -ne "\n\n\n"
	sleep 1


	cd /home/whop/panel
	git clone $6


	cd /home/whop/panel/whop-panel/public/
	ln -s /usr/share/phpMyAdmin phpMyAdmin
	ln -s /usr/share/phpMyAdmin phpmyadmin
	ln -s /usr/share/roundcubemail webmail

	cd /home/whop/panel/whop-panel
	git reset --hard $7
	cd /home/whop/panel/
	chown -R $2:$2 whop-panel

	yes | cp /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php.ori /home/whop/panel/whop-panel/database/seeds/UserTableSeeder.php
	yes | cp /home/whop/panel/whop-panel/.env.example /home/whop/panel/whop-panel/.env
	forever start /home/whop/panel/whop-node-installer/start.js

	ReplaceText "_DBUSERNAME_" "$2" /home/whop/panel/whop-panel/.env
	ReplaceText "_DBPASSWORD_" "$3" /home/whop/panel/whop-panel/.env
	ReplaceText "_REDISPASSWORD_" "$4" /home/whop/panel/whop-panel/.env

	cd /home/whop/panel/whop-panel
	php artisan key:generate
	php artisan key:nodekey
	ReplaceText "IP_ADDRESS=null" "IP_ADDRESS=$ipaddress" /home/whop/panel/whop-panel/.env
	ReplaceText "WHOP_VERSION=null" "WHOP_VERSION=$8" /home/whop/panel/whop-panel/.env

	cd /home/whop/panel/
	chown -R $2:$2 whop-panel

	cd $1
}

function PanelNodeConfig {
	
	echo "requirepass $4" >> /etc/redis.conf
	service redis restart

	cd $1
}