#!/bin/bash

#####################################
# All the include script will be here
#####################################


function InstallAll {
	clear
	echo -ne "\n\n\n"
	echo -ne "##########################\n"
	colorMsg "PART 3: Install Dependency" green
	echo -ne "##########################"
	echo -ne "\n\n\n"
	sleep 1

	cd $1
	cp assets/packages/jq /usr/bin/jq

	# MariaDB 10
	wget http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
	wget -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

	rpm -i epel-release-6-8.noarch.rpm
	rpm -i remi-release-6.rpm
	rm -f epel-* remi-*

	#Replace MySQL with Maria 10
	mariaRepo="
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.0/centos6-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
"

	echo -ne "$mariaRepo" > /etc/yum.repos.d/MariaDB.repo


	rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
	rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
	rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm


	yum install MariaDB-server MariaDB-client gcc patch make libtool openssl openssl-devel pcre pcre-devel zlib zlib-devel pam_mysql php56w-fpm php56w-odbc php56w-intl php56w-tidy php56w-mcrypt php56w-opcache php56w-mssql php56w-pear php56w-pgsql php56w-common php56w-gd php56w-devel php56w php56w-mbstring php56w-cli php56w-imap php56w-snmp php56w-pdo php56w-xml php56w-xmlrpc php56w-pecl-geoip php56w-pecl-imagick php56w-ldap php56w-mysqlnd nodejs npm xterm php56w-pecl-memcached php56w-pecl-apcu php56w-process redis php56w-pecl-redis unzip python-setuptools expect pdns pdns-backend-mysql postfix mailx dovecot dovecot-mysql dovecot-pigeonhole pure-ftpd spamassassin amavisd-new clamav clamav-devel clamd fail2ban db4-devel cyrus-sasl cyrus-sasl-devel openldap openldap-devel mysql-devel cmake bc -y
	easy_install supervisor
	npm install forever -g
	service mysql start
}



function InstallPHPMyAdmin {
	clear
	echo -ne "\n\n\n"
	echo -ne "##########################\n"
	colorMsg "PART 4: Install phpMyAdmin" green
	echo -ne "##########################"
	echo -ne "\n\n\n"
	sleep 1

	cd $1
	cd assets/packages
	tar -zxvf phpMyAdmin-4.4.10-all-languages.tar.gz
	mv phpMyAdmin-4.4.10-all-languages /usr/share/phpMyAdmin

	chown -R whop:whop /usr/share/phpMyAdmin

	cd $1
}


function InstallWebServer {
	clear
	echo -ne "\n\n\n"
	echo -ne "#####################\n"
	colorMsg "PART 5: Install Nginx" green
	echo -ne "#####################"
	echo -ne "\n\n\n"
	sleep 3


	cd /opt/.WHoP_Files
	wget $2
	tar -zxvf $3.tar.gz
	cd $3

	ReplaceText "Server: nginx" "Server: WHoP-nginx" src/http/ngx_http_header_filter_module.c
	ReplaceText "Server: \" NGINX_VER" "Server: WHoP-nginx\"" src/http/ngx_http_header_filter_module.c



	#add pagespeed module
	cd /opt/.WHoP_Files
	wget $5 -O pagespeed-$6.zip
	unzip pagespeed-$6.zip
	cd ngx_pagespeed-release-$6-beta
	wget https://dl.google.com/dl/page-speed/psol/$6.tar.gz
	tar -xzvf $6.tar.gz


	# add naxsi but no config for Arowana. 
	cd /opt/.WHoP_Files
	wget $4 -O naxsi.tar.gz
	mkdir naxsi
	tar -zxvf naxsi.tar.gz -C naxsi --strip-components 1


	#make nginx
	cd /opt/.WHoP_Files/$3

	./configure --prefix=/usr/share/nginx --sbin-path=/usr/local/sbin --conf-path=/etc/nginx/nginx.conf --group=nginx --user=nginx --pid-path=/var/run/nginx.pid --with-http_spdy_module --with-http_stub_status_module --with-http_ssl_module --with-pcre --with-file-aio --with-http_realip_module --with-http_addition_module --with-http_flv_module --with-http_mp4_module --with-http_gunzip_module --with-http_gzip_static_module --with-ipv6 --add-module=../naxsi/naxsi_src --add-module=../ngx_pagespeed-release-$6-beta
	make
	make install

	cd /opt/.WHoP_Files/naxsi/naxsi_config
	cp naxsi_core.rules /etc/nginx/naxsi_core.rules


	#install phalcon
	cd /opt/.WHoP_Git
	git clone --depth=1 git://github.com/phalcon/cphalcon.git
	cd cphalcon/build
	./install
	echo "extension=phalcon.so" > /etc/php.d/phalcon.ini

	

	cd $1
}

function InstallPostFixFTPD {
	clear
	echo -ne "\n\n\n"
	echo -ne "##################################\n"
	colorMsg "PART 6: Roundcube & Update Postfix" green
	echo -ne "##################################"
	echo -ne "\n\n\n"
	sleep 3

	cd /opt/.WHoP_Files
	wget $2 -O roundcubemail.tar.gz
	mkdir roundcubemail
	tar -zxvf roundcubemail.tar.gz -C roundcubemail --strip-components 1
	cp -r /opt/.WHoP_Files/roundcubemail /usr/share/roundcubemail
	
	ReplaceText "distroverpkg=centos-release" "distroverpkg=centos-release\nexclude=postfix*" /etc/yum.conf
	
	cd $1
	cd assets/packages
	cp postfix-2.11.6.tar.gz /opt/.WHoP_Files/
	cp postsrsd-master.zip /opt/.WHoP_Files/
	cd /opt/.WHoP_Files/
	
	tar -zxvf postfix-2.11.6.tar.gz
	cd postfix-2.11.6
	make clean
	make tidy
	make makefiles CCARGS='-fPIC -DUSE_TLS -DUSE_SSL -DUSE_SASL_AUTH -DUSE_CYRUS_SASL -DPREFIX=\\"/usr\\" -DHAS_LDAP -DLDAP_DEPRECATED=1 -DHAS_PCRE -I/usr/include/openssl -DHAS_MYSQL -I/usr/include/mysql -I/usr/include/sasl  -I/usr/include' AUXLIBS='-L/usr/lib64 -L/usr/lib64/openssl -lssl -lcrypto -L/usr/lib64/mysql -lmysqlclient -L/usr/lib64/sasl2 -lsasl2 -lpcre -lz -lm -lldap -llber -Wl,-rpath,/usr/lib64/openssl -pie -Wl,-z,relro' OPT='-O' DEBUG='-g'
	make upgrade
	
	cd /opt/.WHoP_Files
	unzip postsrsd-master.zip
	cd postsrsd-master
	make
	make install

	cd $1
}

function InstallPHPSecurity {
	clear
	echo -ne "\n\n\n"
	echo -ne "#############################\n"
	colorMsg "PART 7: Suhosin, CSF Firewall" green
	echo -ne "#############################"
	echo -ne "\n\n\n"
	sleep 3


	cd /opt/.WHoP_Git
	git clone $2
	cd suhosin
	git checkout $3
	phpize
	./configure
	make
	make install
	echo -ne "extension=suhosin.so\nsuhosin.session.encrypt = 0\nsuhosin.executor.include.whitelist = phar" > /etc/php.d/suhosin.ini


	wget http://www.configserver.com/free/csf.tgz -O /opt/csf.tgz
	cd /opt/
	tar -zxvf csf.tgz
	cd /opt/csf
	sh install.sh

	cd $1
}