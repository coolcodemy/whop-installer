#!/bin/bash

function ReplaceWholeLine {
	# $1 = String to search
	# $2 = New String
	# $3 = File location
	sed -i "s/$1.*/$2/" $3
}

function ReplaceText {
    # $1 = String to search
    # $2 = New String
    # $3 = File location
    sed -i "s/$1/$2/g" $3
}

function NewUser {
	useradd -s /bin/false -d /bin/null -c "$2" $1
}

function NewFolder {
	mkdir -p $1
}


function colorMsg {
    local exp=$1;
    local color=$2;
    if ! [[ $color =~ '^[0-9]$' ]] ; then
       case $(echo $color | tr '[:upper:]' '[:lower:]') in
        black) color=0 ;;
        red) color=1 ;;
        green) color=2 ;;
        yellow) color=3 ;;
        blue) color=4 ;;
        magenta) color=5 ;;
        cyan) color=6 ;;
        white|*) color=7 ;; # white or invalid color
       esac
    fi
    tput setaf $color;
    echo $exp;
    tput sgr0;
}

function RandomString
{
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-$1} | head -n 1
}

function RandomStringSymbol
{
    cat /dev/urandom | tr -dc 'a-zA-Z0-9*()+?.,' | fold -w ${1:-$1} | head -n 1
}