#!/bin/bash

#####################################
# All the include script will be here
#####################################

function ConfigureMySQL {
	clear
	echo -ne "\n\n\n"
	echo -ne "#######################\n"
	colorMsg "PART 8: Configure MySQL" green
	echo -ne "#######################"
	echo -ne "\n\n\n"

	totalMemory=`free|awk '/^Mem:/{print $2}'`
	totalMemoryMB=$(($totalMemory/1024))
	mysqlMemory=$((30*$totalMemoryMB/100))

	if [[ $mysqlMemory == 0 ]];
		then
			mysqlMemory=1024
	fi


	mysql -u root --execute="CREATE DATABASE WHoP_Master"
	mysql -u root --execute="CREATE DATABASE WHoP_Roundcubemail"
	mysql -u root WHoP_Master < assets/settings/WHoP_Master.sql
	mysql -u root WHoP_Roundcubemail < assets/settings/roundcube.sql
	mysql -u root --execute="GRANT ALL PRIVILEGES on *.* TO '$1'@'localhost' IDENTIFIED BY '$2'"
	mysql -u root --execute="GRANT ALL PRIVILEGES on *.* TO '$1'@'127.0.0.1' IDENTIFIED BY '$2'"
	mysql -u root --execute="GRANT GRANT OPTION on *.* TO '$1'@'localhost'"
	mysql -u root --execute="GRANT GRANT OPTION on *.* TO '$1'@'127.0.0.1'"
	mysql -u root --execute="FLUSH PRIVILEGES"

	chown -R whop:whop /usr/share/phpMyAdmin


	ReplaceText "\[mariadb\]" "\[mariadb\]\nlog-error=\/var\/log\/mariadb.log\nlog-warning = 2\ninnodb_buffer_pool_size = "$mysqlMemory"M\ninnodb_file_per_table = 1\nmax_connections = 1000\nmax_user_connections = 500\nskip-name-resolve\nquery_cache_size = 80M\nquery_cache_type = 1\nquery_cache_limit = 2M\nquery_cache_min_res_unit = 2k\nthread_cache_size = 60\ninnodb_buffer_pool_instances = 8" /etc/my.cnf.d/server.cnf
}

function ConfigurePDNS {
	clear
	echo -ne "\n\n\n"
	echo -ne "######################\n"
	colorMsg "PART 9: Configure PDNS" green
	echo -ne "######################"
	echo -ne "\n\n\n"


	conf="/etc/pdns/pdns.conf"
	ReplaceWholeLine "launch=" "launch=gmysql" $conf
	echo "allow-recursion=127.0.0.1" >> $conf
	echo "disable-axfr=yes" >> $conf
	echo "disable-tcp=no" >> $conf
	echo "guardian=yes" >> $conf
	echo "master=no" >> $conf
	echo "webserver=no" >> $conf
	echo "receiver-threads=8" >> $conf
	echo "gmysql-host=127.0.0.1" >> $conf
	echo "gmysql-user=$1" >> $conf
	echo "gmysql-dbname=WHoP_Master" >> $conf
	echo "gmysql-password=$2" >> $conf
}

function ConfigureNginx {
	clear
	echo -ne "\n\n\n"
	echo -ne "##################################\n"
	colorMsg "PART 10: Configure Nginx & PHP-FPM" green
	echo -ne "##################################"
	echo -ne "\n\n\n"

	cd $1
	cp assets/settings/nginx.dat /etc/nginx/nginx.conf
	cp assets/settings/whopwaf.rules /etc/nginx/whopwaf.rules
	NewFolder /var/log/nginx
	NewFolder /var/cache/nginx

	chown -R nginx:nginx /var/cache/nginx

	mkdir -p /home/whop/panel/{cache,session,default_page}
	chmod +t /home/whop/panel/cache
	chown -R whop:whop /home/whop

	cp assets/settings/nginx-initd.dat /etc/init.d/nginx
	chmod +x /etc/init.d/nginx

	cp assets/settings/whop-installer\|nginx.conf /home/whop/hosting/whop/whop-installer\|nginx.conf


	cd $1


	ReplaceText "user = apache" "user = whop" /etc/php-fpm.d/www.conf
	ReplaceText "group = apache" "group = whop" /etc/php-fpm.d/www.conf

	ReplaceWholeLine ";listen.owner =" "listen.owner = whop" /etc/php-fpm.d/www.conf
	ReplaceWholeLine ";listen.group =" "listen.group = whop" /etc/php-fpm.d/www.conf

	#disable apcu
	#mv /etc/php.d/apcu.ini /etc/php.d/apcu.ini.disabled

	chmod 755 /home/whop
	chmod 755 /home/whop/panel

	cd $1
}


function ConfigurePHPSecurity {
	clear
	echo -ne "\n\n\n"
	echo -ne "###############################\n"
	colorMsg "PART 11: Configure PHP Security" green
	echo -ne "###############################"
	echo -ne "\n\n\n"
	sleep 1

	ReplaceText "expose_php = On" "expose_php = Off" /etc/php.ini
	ReplaceText "display_errors = On" "display_errors = Off" /etc/php.ini
	ReplaceText "allow_url_include = On" "allow_url_include = Off" /etc/php.ini
	ReplaceText "magic_quotes_gpc = On" "magic_quotes_gpc = Off" /etc/php.ini
	ReplaceText "expose_php = On" "expose_php = Off" /etc/php.ini
	ReplaceText "max_input_time = 60" "max_input_time = 30" /etc/php.ini

	ReplaceText "zlib.output_compression = Off" "zlib.output_compression = On" /etc/php.ini

	ReplaceWholeLine "memory_limit = " "memory_limit = 256M" /etc/php.ini
	ReplaceWholeLine "post_max_size = " "post_max_size = 100M" /etc/php.ini
	ReplaceWholeLine "upload_max_filesize =" "upload_max_filesize = 100M" /etc/php.ini
	ReplaceWholeLine ";cgi.fix_pathinfo=" "cgi.fix_pathinfo = 1" /etc/php.ini
	ReplaceWholeLine ";cgi.fix_pathinfo =" "cgi.fix_pathinfo = 1" /etc/php.ini


	ReplaceWholeLine "file_uploads = Off" "file_uploads = On" /etc/php.ini
	ReplaceWholeLine "sql.safe_mode =" "sql.safe_mode = Off" /etc/php.ini
	ReplaceWholeLine "disable_functions" "disable_functions = getmyuid,passthru,leak,listen,diskfreespace,tmpfile,link,ignore_user_abord,shell_exec,dl,set_time_limit,exec,system,highlight_file,source,show_source,fpassthru,virtual,posix_ctermid,posix_getcwd,posix_getegid,posix_geteuid,posix_getgid,posix_getgrgid,posix_getgrnam,posix_getgroups,posix_getlogin,posix_getpgid,posix_getpgrp,posix_getpid,posix,_getppid,posix_getpwuid,posix_getrlimit,posix_getsid,posix_getuid,posix_isatty,posix_kill,posix_mkfifo,posix_setegid,posix_seteuid,posix_setgid,posix_setpgid,posix_setsid,posix_setuid,posix_times,posix_ttyname,posix_uname,proc_nice,proc_terminate,escapeshellcmd,ini_alter,popen,pcntl_exec,socket_accept,socket_bind,socket_clear_error,socket_close,socket_connect,curl_multi_exec,parse_ini_file,proc_close,proc_get_status,proc_open,symlink,posix_geteuid,ini_alter,socket_listen,socket_create_listen,socket_read,socket_create_pair,stream_socket_server" /etc/php.ini
}

function ConfigurePureFTPD {
	clear
	echo -ne "\n\n\n"
	echo -ne "############################\n"
	colorMsg "PART 12: Configure Pure-FTPD" green
	echo -ne "############################"
	echo -ne "\n\n\n"
	sleep 1

	# mv /etc/pure-ftpd/pure-ftpd.conf /etc/pure-ftpd/pure-ftpd.conf.bak
	# cp assets/settings/pure-ftpd.dat /etc/pure-ftpd/pure-ftpd.conf

	mv /etc/pure-ftpd/pureftpd-mysql.conf /etc/pure-ftpd/pureftpd-mysql.conf.bak
	cp assets/settings/pureftpd-mysql.dat /etc/pure-ftpd/pureftpd-mysql.conf

	configFile="/etc/pure-ftpd/pure-ftpd.conf"

	ReplaceWholeLine "MaxClientsNumber" "MaxClientsNumber 500" $configFile
	ReplaceWholeLine "VerboseLog" "VerboseLog yes" $configFile
	ReplaceWholeLine "DisplayDotFiles" "DisplayDotFiles no" $configFile
	ReplaceWholeLine "NoAnonymous" "NoAnonymous yes" $configFile
	ReplaceText "# MySQLConfigFile" "MySQLConfigFile" $configFile
	ReplaceWholeLine "# PassivePortRange" "PassivePortRange 30002 30502" $configFile
	ReplaceWholeLine "#CreateHomeDir" "CreateHomeDir no" $configFile
	ReplaceText "# TLS" "TLS" $configFile
	ReplaceWholeLine "# FileSystemCharset" "FileSystemCharset UTF-8" $configFile
	echo "FortunesFile	/etc/pure-ftpd/banner.txt" >> $configFile

	ReplaceText "_WHOPUSER_" "$1" /etc/pure-ftpd/pureftpd-mysql.conf
	ReplaceText "_WHOPPASSWORD_" "$2" /etc/pure-ftpd/pureftpd-mysql.conf

	echo -ne "WHoP FTP\n\nPlease make sure you enter right username and password or you will be blocked for several minutes" > /etc/pure-ftpd/banner.txt

	#tls
	mkdir /etc/ssl/private/
	cp /home/whop/hosting/whop/SSL/whop.crt /etc/ssl/private/pure-ftpd.pem
	cp /home/whop/hosting/whop/SSL/whop.key /etc/ssl/private/pure-ftpd.key

	chmod 600 /etc/ssl/private/pure-ftpd.*
}

function ConfigurePostfix {
	clear
	echo -ne "\n\n\n"
	echo -ne "##########################\n"
	colorMsg "PART 13: Configure Postfix" green
	echo -ne "##########################"
	echo -ne "\n\n\n"
	sleep 1

	mkdir -p /etc/postfix/whop

	virtualDomainConfig="
user = $1
password = $2
dbname = WHoP_Master
query = SELECT name AS virtual FROM records WHERE name='%s' AND type='A'
hosts = 127.0.0.1
"
	echo -ne "$virtualDomainConfig" > /etc/postfix/whop/mail_virtualdomain.cf



	virtualForwardingConfig="
user = $1
password = $2
dbname = WHoP_Master
query = SELECT destination FROM mail_forwardings WHERE source='%s' AND disable=0 AND masterDisable=0
hosts = 127.0.0.1
"
	echo -ne "$virtualForwardingConfig" > /etc/postfix/whop/mail_virtualforwardings.cf



	virtualMailboxConfig="
user = $1
password = $2
dbname = WHoP_Master
query = SELECT CONCAT(SUBSTRING_INDEX(email,'@',-1),'/',SUBSTRING_INDEX(email,'@',1),'/') FROM mail_users WHERE email='%s' AND disable=0 AND masterDisable=0
hosts = 127.0.0.1
"
	echo -ne "$virtualMailboxConfig" > /etc/postfix/whop/mail_virtualmailbox.cf



	virtualEmail2Email="
user = $1
password = $2
dbname = WHoP_Master
query = SELECT email FROM mail_users WHERE email='%s' AND disable=0 AND masterDisable=0
hosts = 127.0.0.1
"
	echo -ne "$virtualEmail2Email" > /etc/postfix/whop/mail_virtualemail2email.cf


	uid=`id -u whopemail`
	gid=`id -g whopemail`


	mkdir /var/whop-mailbox
	chown -R whopemail:whopemail /var/whop-mailbox

	postconf -e 'mydestination = localhost'
	postconf -e 'mynetworks = 127.0.0.0/8'
	postconf -e 'virtual_alias_domains ='
	postconf -e 'virtual_alias_maps = proxy:mysql:/etc/postfix/whop/mail_virtualforwardings.cf,proxy:mysql:/etc/postfix/whop/mail_virtualemail2email.cf'
	postconf -e 'virtual_mailbox_domains = proxy:mysql:/etc/postfix/whop/mail_virtualdomain.cf'
	postconf -e 'virtual_mailbox_maps = proxy:mysql:/etc/postfix/whop/mail_virtualmailbox.cf'
	postconf -e 'virtual_mailbox_base = /var/whop-mailbox'
	postconf -e "virtual_uid_maps = static:$uid"
	postconf -e "virtual_gid_maps = static:$gid"
	postconf -e 'smtp_tls_note_starttls_offer = yes'
	postconf -e 'smtp_use_tls = yes'
	postconf -e 'smtpd_tls_auth_only = no'
	postconf -e 'smtpd_sasl_type = dovecot'
	postconf -e 'smtpd_sasl_path = private/auth'
	postconf -e 'smtpd_sasl_auth_enable = yes'
	postconf -e 'smtpd_sasl_authenticated_header = yes'
	postconf -e 'smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination, reject_rbl_client dnsbl.sorbs.net, reject_rbl_client zen.spamhaus.org, reject_rbl_client bl.spamcop.net, permit'
	postconf -e 'smtpd_use_tls = yes'
	postconf -e 'smtpd_tls_cert_file = /home/whop/hosting/whop/SSL/whop.crt'
	postconf -e 'smtpd_tls_key_file = /home/whop/hosting/whop/SSL/whop.key'
	postconf -e 'broken_sasl_auth_clients = yes'
	postconf -e 'dovecot_destination_recipient_limit = 1'
	postconf -e 'virtual_transport = dovecot'
	postconf -e 'proxy_read_maps = $local_recipient_maps $mydestination $virtual_alias_maps $virtual_alias_domains $virtual_mailbox_maps $virtual_mailbox_domains $relay_recipient_maps $relay_domains $canonical_maps $sender_canonical_maps $recipient_canonical_maps $relocated_maps $transport_maps $mynetworks'
	postconf -e 'inet_interfaces = all'
	postconf -e 'header_checks = regexp:/etc/postfix/header_checks'
	postconf -e 'receive_override_options = no_unknown_recipient_checks'
	postconf -e 'authorized_submit_users = root, whopemail, spamd'
	postconf -e 'content_filter = amavisfeed:[127.0.0.1]:10024'
	postconf -e 'inet_protocols = ipv4'
	postconf -e "sender_canonical_maps = tcp:127.0.0.1:10001"
	postconf -e "sender_canonical_classes = envelope_sender"
	postconf -e "recipient_canonical_maps = tcp:127.0.0.1:10002"
	postconf -e "recipient_canonical_classes = envelope_recipient,header_recipient"

	submission="
submission inet n	-	n	-	-	smtpd
  -o smtpd_tls_security_level=encrypt
  -o smtpd_sasl_auth_enable=yes
  -o smtpd_client_restrictions=permit_sasl_authenticated,reject
  -o milter_macro_daemon_name=ORIGINATING
"

	lda="
dovecot   unix  -       n       n       -       -       pipe
  flags=DRhu user=whopemail:whopemail argv=/usr/libexec/dovecot/dovecot-lda -f \${sender} -d \${recipient}
"

# 	dsrescue="
# dsrescue   unix  -       n       n       -       -       pipe
#   flags=Rh user=whopemail:whopemail argv=/usr/bin/ds-rescue -f \${sender} -- \${recipient}
# "

	spamass="
spamassassin unix - n n - - pipe flags=R user=spamd argv=/usr/bin/spamc -e /usr/sbin/sendmail -oi -f \${sender} \${recipient}
"

	amavis="
amavisfeed unix -       -       n       -       2       lmtp
  -o lmtp_data_done_timeout=1200
  -o lmtp_send_xforward_command=yes

127.0.0.1:10025 inet n    -       n       -       -     smtpd
  -o content_filter=
  -o smtpd_delay_reject=no
  -o smtpd_client_restrictions=permit_mynetworks,reject
  -o smtpd_helo_restrictions=
  -o smtpd_sender_restrictions=
  -o smtpd_recipient_restrictions=permit_mynetworks,reject
  -o smtpd_data_restrictions=reject_unauth_pipelining
  -o smtpd_end_of_data_restrictions=
  -o smtpd_restriction_classes=
  -o mynetworks=127.0.0.0/8
  -o smtpd_error_sleep_time=0
  -o smtpd_soft_error_limit=1001
  -o smtpd_hard_error_limit=1000
  -o smtpd_client_connection_count_limit=0
  -o smtpd_client_connection_rate_limit=0
  -o receive_override_options=no_header_body_checks,no_unknown_recipient_checks,no_milters
  -o local_header_rewrite_clients=
  -o smtpd_milters=
  -o local_recipient_maps=
  -o relay_recipient_maps="

	echo -ne "$submission" >> /etc/postfix/master.cf
	echo -ne "$lda" >> /etc/postfix/master.cf
	echo -ne "$dsrescue" >> /etc/postfix/master.cf
	echo -ne "$amavis" >> /etc/postfix/master.cf
	#echo -ne "$spamass" >> /etc/postfix/master.cf

	ReplaceWholeLine "smtp      inet" "smtp	  inet  n	-	n	-	-	smtpd -o smtpd_tls_security_level=may -o smtpd_sasl_auth_enable=yes -o smtpd_client_restrictions=permit_sasl_authenticated" /etc/postfix/master.cf

	# echo "/^rescue@whop.io/   dsrescue:" > /etc/postfix/dsrescue.regexp

	headerFilter="
/^Received:/    IGNORE
/^X-Originating-IP:/    IGNORE
/^X-Mailer:/            IGNORE
/^X-PHP-Originating-Script:/    IGNORE
/^User-Agent:/  REPLACE User-Agent: WHoP
"
	echo -ne "$headerFilter" >> /etc/postfix/header_checks
}

function ConfigureDovecot {
	clear
	echo -ne "\n\n\n"
	echo -ne "##########################\n"
	colorMsg "PART 14: Configure Dovecot" green
	echo -ne "##########################"
	echo -ne "\n\n\n"
	sleep 1

	uid=`id -u whopemail`
	gid=`id -g whopemail`

	dovewhop="
protocol lda {
    auth_socket_path = /var/run/dovecot/auth-master
    postmaster_address = noreply@whop.io
    mail_plugins = quota sieve
}


service auth {
	unix_listener /var/spool/postfix/private/auth {
		mode = 0600
		user = postfix
		group = postfix
	}

	unix_listener auth-userdb {
		mode = 0600
		user = whopemail
		group = whopemail
	}
	unix_listener auth-master {
		mode = 0600
		user = whopemail
		group = whopemail
	}
}

protocol imap {
    mail_plugins = quota imap_quota autocreate
}
protocol pop3 {
    mail_plugins = quota
}

plugin {
  quota = maildir:User quota
  quota_rule2 = Trash:storage=+10%%
  trash = /etc/dovecot/trash.conf
  sieve_global_path = /etc/dovecot/sieve/globalfilter.sieve
  autocreate = Trash
  autocreate2 = Spam
  autosubscribe = Trash
  autosubscribe2 = Spam
}
"

	ReplaceWholeLine "#protocols = imap pop3" "protocols = imap pop3" /etc/dovecot/dovecot.conf

	cp assets/settings/whopdove-sql.dat /etc/dovecot/whopdove-sql.conf

	ReplaceText "_DBUSER_" "$1" /etc/dovecot/whopdove-sql.conf
	ReplaceText "_DBPASSWORD_" "$2" /etc/dovecot/whopdove-sql.conf
	ReplaceText "_UID_" "$uid" /etc/dovecot/whopdove-sql.conf
	ReplaceText "_GID_" "$gid" /etc/dovecot/whopdove-sql.conf

	cp assets/settings/auth-sql.conf.ext /etc/dovecot/conf.d/auth-sql.conf.ext

	echo -e "log_path = /var/log/dovecot.log" >> /etc/dovecot/dovecot.conf
	echo -e "mail_location = maildir:/var/whop-mailbox/%d/%n" >> /etc/dovecot/dovecot.conf
	echo -e "ssl = required" >> /etc/dovecot/dovecot.conf
	echo -e "ssl_cert = </home/whop/hosting/whop/SSL/whop.crt" >> /etc/dovecot/dovecot.conf
	echo -e "ssl_key = </home/whop/hosting/whop/SSL/whop.key" >> /etc/dovecot/dovecot.conf
	echo -e "$dovewhop" >> /etc/dovecot/dovecot.conf


	ReplaceWholeLine "!include auth-system.conf" "#!include auth-system.conf.ext" /etc/dovecot/conf.d/10-auth.conf
	ReplaceWholeLine "#!include auth-sql.conf" "!include auth-sql.conf.ext" /etc/dovecot/conf.d/10-auth.conf

	ReplaceText "ssl_cert =" "#ssl_cert =" /etc/dovecot/conf.d/10-ssl.conf
	ReplaceText "ssl_key =" "#ssl_key =" /etc/dovecot/conf.d/10-ssl.conf

	globalSieve="
require \"fileinto\";
  if exists \"X-Spam-Flag\" {
          if header :contains \"X-Spam-Flag\" \"NO\" {
          } else {
          fileinto \"Spam\";
          stop;
          }
  }
  if header :contains \"subject\" [\"***SPAM***\"] {
    fileinto \"Spam\";
    stop;
  }
"

	mkdir /etc/dovecot/sieve
	echo -e "$globalSieve" > /etc/dovecot/sieve/globalfilter.sieve
	chown whopemail:whopemail /etc/dovecot/sieve/
}

function ConfigureRoundcube {
	clear
	echo -ne "\n\n\n"
	echo -ne "############################\n"
	colorMsg "PART 15: Configure Roundcube" green
	echo -ne "############################"
	echo -ne "\n\n\n"
	sleep 1

	cp /usr/share/roundcubemail/config/config.inc.php.sample /usr/share/roundcubemail/config/config.inc.php

	config="/usr/share/roundcubemail/config/config.inc.php"

	ReplaceWholeLine "\$config\['db_dsnw'\] =" "\$config\['db_dsnw'\] = 'mysql:\/\/$1:$2@localhost\/WHoP_Roundcubemail';" $config
	ReplaceWholeLine "\$config\['smtp_port'\] =" "\$config\['smtp_port'\] = 25;" $config
	ReplaceWholeLine "\$config\['smtp_user'\] =" "\$config\['smtp_user'\] = '%u';" $config
	ReplaceWholeLine "\$config\['smtp_pass'\] =" "\$config\['smtp_pass'\] = '%p';" $config
	ReplaceWholeLine "\$config\['default_host'\] =" "\$config\['default_host'\] = 'localhost';" $config
	ReplaceWholeLine "\$config\['smtp_server'\] =" "\$config\['smtp_server'\] = '_DOMAINNAME_';" $config
	ReplaceWholeLine "\$config\['product_name'\] =" "\$config\['product_name'\] = 'Roundcube Webmail - WHoP';" $config
	ReplaceWholeLine "\$config\['des_key'\] =" "\$config\['des_key'\] = '$(RandomString 24)';" $config

	echo -e "\n\$config['spellcheck_engine'] = 'pspell';" >> $config
	echo -e "\$config['identities_level'] = 3;" >> $config


	additionalConfig="
\$config['smtp_conn_options'] = array(
  'ssl'         => array(
    'verify_peer'  => false,
    'verify_depth' => 3,
    'cafile'       => '/etc/ssl/certs/ca-bundle.crt',
  ),
);
"
	
	echo -ne "$additionalConfig" >> /usr/share/roundcubemail/config/config.inc.php

	touch /var/log/roundcubemail

	chown -R whop:whop /usr/share/roundcubemail
	chown -R whop:whop /var/log/roundcubemail
}

function ConfigureSpamassassin {
	clear
	echo -ne "\n\n\n"
	echo -ne "###############################\n"
	colorMsg "PART 16: Configure Spamassassin" green
	echo -ne "###############################"
	echo -ne "\n\n\n"
	sleep 3

	ReplaceWholeLine "required_hits" "#required_hits" /etc/mail/spamassassin/local.cf
	ReplaceWholeLine "report_safe" "report_safe 0" /etc/mail/spamassassin/local.cf
	ReplaceWholeLine "rewrite_header" "rewrite_header Subject ***SPAM***" /etc/mail/spamassassin/local.cf
	echo "required_score 5" >> /etc/mail/spamassassin/local.cf

	groupadd spamd
	useradd -g spamd -s /bin/false -d /var/log/spamassassin spamd
	chown spamd:spamd /var/log/spamassassin

	echo "23 4 */2 * * /usr/bin/sa-update --no-gpg &> /dev/null" | crontab -
	echo "Updating spamassassin..."
	sa-update --no-gpg


	# Configuring amavis and clamav
	ReplaceWholeLine "TCPSocket 3310" "#TCPSocket 3310" /etc/clamd.conf
	ReplaceWholeLine "AllowSupplementaryGroups yes" "AllowSupplementaryGroups true" /etc/clamd.conf
	ReplaceText "\/var\/spool\/amavisd\/clamd.sock" "\/var\/run\/clamav\/clamd.sock" /etc/amavisd/amavisd.conf

	usermod -G amavis clam
	freshclam
}

function ConfigureCSF {
	clear
	echo -ne "\n\n\n"
	echo -ne "###############################\n"
	colorMsg "PART 17: Configure CSF Firewall" green
	echo -ne "###############################"
	echo -ne "\n\n\n"
	sleep 3

	hn=`hostname`

	ReplaceWholeLine "TESTING =" "TESTING = \"0\"" /etc/csf/csf.conf
	ReplaceWholeLine "TCP_IN =" "TCP_IN = \"20,21,22,25,53,80,110,143,443,587,993,995,3025,3306,30002:30502\"" /etc/csf/csf.conf
	ReplaceWholeLine "TCP_OUT =" "TCP_OUT = \"20,21,22,25,53,80,110,113,443,465,587,993,995\"" /etc/csf/csf.conf
	ReplaceWholeLine "UDP_OUT =" "UDP_OUT = \"20,21,53,113,123,143,587\"" /etc/csf/csf.conf
	ReplaceWholeLine "SMTP_ALLOWUSER =" "SMTP_ALLOWUSER = \"postfix\"" /etc/csf/csf.conf
	ReplaceWholeLine "PT_USERMEM =" "PT_USERMEM = \"500\"" /etc/csf/csf.conf
	ReplaceWholeLine "SMTP_BLOCK = \"0\"" "SMTP_BLOCK = \"1\"" /etc/csf/csf.conf
	ReplaceWholeLine "PT_FORKBOMB = \"0\"" "PT_FORKBOMB = \"300\"" /etc/csf/csf.conf
	ReplaceWholeLine "PS_INTERVAL = \"0\"" "PS_INTERVAL = \"300\"" /etc/csf/csf.conf
	ReplaceWholeLine "AT_ALERT = \"2\"" "AT_ALERT = \"1\"" /etc/csf/csf.conf
	ReplaceWholeLine "LF_ALERT_FROM = \"\"" "LF_ALERT_FROM = \"whopcsf@"$hn"\"" /etc/csf/csf.conf


	echo "exe:/usr/libexec/postfix/smtpd" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/mysqld" >> /etc/csf/csf.pignore
	echo "exe:/usr/sbin/clamd" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/tlsmgr" >> /etc/csf/csf.pignore
	echo "exe:/usr/sbin/pdns_server" >> /etc/csf/csf.pignore
	echo "exe:/usr/local/sbin/nginx" >> /etc/csf/csf.pignore
	echo "exe:/usr/bin/perl" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/dovecot/anvil" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/qmgr" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/pickup" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/cleanup" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/proxymap" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/dovecot/auth" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/smtp" >> /etc/csf/csf.pignore
	echo "exe:/usr/sbin/hald" >> /etc/csf/csf.pignore
	echo "exe:/usr/sbin/redis-server" >> /etc/csf/csf.pignore
	echo "exe:/usr/sbin/mysqld" >> /etc/csf/csf.pignore
	echo "exe:/usr/sbin/php-fpm" >> /etc/csf/csf.pignore
	echo "exe:/usr/bin/dbus-launch" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/hald-addon-acpi" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/rtkit-daemon" >> /etc/csf/csf.pignore
	echo "exe:/sbin/rpc.statd" >> /etc/csf/csf.pignore
	echo "exe:/usr/libexec/postfix/lmtp" >> /etc/csf/csf.pignore
	echo "exe:/usr/bin/php" >> /etc/csf/csf.pignore
	echo "exe:/usr/bin/freshclam" >> /etc/csf/csf.pignore
	echo "exe:/sbin/rpcbind" >> /etc/csf/csf.pignore
	echo "exe:/usr/local/sbin/postsrsd" >> /etc/csf/csf.pignore
	echo "user:whop" >> /etc/csf/csf.pignore
	echo "user:postfix" >> /etc/csf/csf.pignore
	echo "user:rpc" >> /etc/csf/csf.pignore

}


function ConfigureFail2Ban {
	clear
	echo -ne "\n\n\n"
	echo -ne "###########################\n"
	colorMsg "PART 18: Configure Fail2Ban" green
	echo -ne "###########################"
	echo -ne "\n\n\n"
	sleep 1

	cd $1
	cp assets/settings/fail2banjail.local /etc/fail2ban/jail.local
	cp assets/settings/fail2ban-mysqlfilter.conf /etc/fail2ban/filter.d/whop-mysqlauth.conf
	cp assets/settings/fail2ban-nodefilter.conf /etc/fail2ban/filter.d/whop-nodeauth.conf

	touch /var/log/whop-node-auth.log
}


function WHoPJail {
	clear
	echo -ne "\n\n\n"
	echo -ne "##########################\n"
	colorMsg "PART 19: Preparing Jailkit" green
	echo -ne "##########################"
	echo -ne "\n\n\n"
	sleep 3

	cd $1

	groupadd whopjail


	cp assets/jail/whop-jail /usr/bin/whop-jail
	cp assets/jail/whop-license /usr/bin/whop-license
	cp assets/jail/whop-updatejail /usr/bin/whop-updatejail
	cp assets/jail/whop-removeuser /usr/bin/whop-removeuser
	cp assets/jail/whop-repquota /usr/bin/whop-repquota
	cp assets/jail/whop-tool /usr/bin/whop-tool

	chmod +x /usr/bin/whop-jail
	chmod +x /usr/bin/whop-license
	chmod +x /usr/bin/whop-updatejail
	chmod +x /usr/bin/whop-removeuser
	chmod +x /usr/bin/whop-repquota

	cp assets/packages/jailkit.tar.gz /opt/jailkit.tar.gz
	cd /opt
	tar -zxvf jailkit.tar.gz

	cd /opt/jailkit
	./configure
	make
	make install

	cd $1

	mkdir -p /usr/share/whop/license
	cp -r assets/cron /usr/share/whop/
	cp -r assets/overquota /usr/share/whop/

	mkdir /var/www/whop/
	cp assets/html/quotaexceeded.html /var/www/whop/
	cp assets/html/whopwafblocked.html /var/www/whop/


	cd $1

	echo "Updating mlocate database. This might take a while..."
	updatedb
}


function RegisterService {
	clear
	echo -ne "\n\n\n"
	echo -ne "#######################################\n"
	colorMsg "PART 20: Registering WHoP to the system" green
	echo -ne "#######################################"
	echo -ne "\n\n\n"
	sleep 1


	rand=`echo $RANDOM % 10 + 1 | bc`

	echo "0 0 */5 * * /usr/share/whop/cron/noderediscron.sh" >> /var/spool/cron/root
	echo "0 */1 * * * /usr/share/whop/cron/quotamanager.sh" >> /var/spool/cron/root
	echo "0 1 1 * * /usr/share/whop/cron/quotareset.sh" >> /var/spool/cron/root
	echo "15 */6 * * * /usr/share/whop/cron/accounttermination.sh" >> /var/spool/cron/root
	echo "0 0 1 * * /usr/share/whop/cron/logcleaner.sh" >> /var/spool/cron/root
	echo "15 $rand * * * /usr/share/whop/cron/autoupdate.sh" >> /var/spool/cron/root

	cd $1


	# Add supervisor config
	cp assets/settings/supervisord-initd.dat /etc/init.d/supervisord
	chmod +x /etc/init.d/supervisord

	echo_supervisord_conf > /etc/supervisord.conf

	supervisorinclude="
[include]
files = /home/whop/hosting/whop/supervisord/*.conf"

	echo -ne "$supervisorinclude" >> /etc/supervisord.conf

	whopsupervisor="
[program:whop-panel-master]
command=php /home/whop/panel/whop-panel/artisan queue:work --daemon --tries=3 --env=production
redirect_stderr=true
autostart=true
autorestart=true
user=whop"

	echo -ne "$whopsupervisor" > /home/whop/hosting/whop/supervisord/whop-panel-master.conf

	csf -r


	#fix for digital ocean, disable imap and then enable back when SOA have been registered
	cd /etc/php.d
	mv imap.ini imap.ini.disable

	service sshd restart

	chkconfig mysql on
	service mysql start

	chkconfig pdns on
	service pdns start

	chkconfig nginx on
	service nginx start

	chkconfig pure-ftpd on
	service pure-ftpd start

	chkconfig spamassassin on
	service spamassassin start

	chkconfig clamd on
	service clamd start

	chkconfig amavisd on
	service amavisd start
	
	chkconfig postsrsd on
	service postsrsd start

	chkconfig postfix on
	service postfix restart

	chkconfig dovecot on
	service dovecot start

	chkconfig php-fpm on
	service php-fpm start

	chkconfig redis on
	service redis start

	chkconfig fail2ban on
	service fail2ban start

	service sshd restart

	chkconfig supervisord on
	service supervisord start

	chkconfig whopquota on

	chown postfix:postfix /var/log/dovecot.log
	chmod 777 /var/log/dovecot.log
}


function ConfigureMysqlRootPassword {
	
	SECURE_MYSQL=$(expect -c "

	set timeout 5
	spawn mysql_secure_installation

	expect \"Enter current password for root (enter for none):\"
	send \"\r\"

	expect \"Change the root password?\"
	send \"y\r\"

	expect \"New password:\"
	send \"$1\r\"

	expect \"Re-enter new password:\"
	send \"$1\r\"

	expect \"Remove anonymous users?\"
	send \"y\r\"

	expect \"Disallow root login remotely?\"
	send \"y\r\"

	expect \"Remove test database and access to it?\"
	send \"y\r\"

	expect \"Reload privilege tables now?\"
	send \"y\r\"

	expect eof
	")

	echo "$SECURE_MYSQL"
}