#!/bin/bash

#####################################
# All the include script will be here
#####################################

function DisableSeLinux {
	# Check selinux
	selinux=`sestatus | grep "SELinux status"`
	selinux=( $selinux )
	if [ "${selinux[2]}" == "enabled" ]
	then
		ReplaceWholeLine "SELINUX=" "SELINUX=disabled" "/etc/selinux/config"
		setenforce 0
	fi
}


# New User
function CreateUser {
	NewUser "whop" "Main WHoP user"
	NewUser "nginx" "nginx user"
	NewUser "whopemail" "Main email user"
}


function Init {
	mkdir /opt/.WHoP_Git
	mkdir /opt/.WHoP_Files
	mkdir /opt/.WHoP_Patch

	sslConfig="$1/assets/settings/openssl.cnf"

	NewFolder "/usr/share/whop/template"
	NewFolder "/home/whop/hosting/whop/SSL"
	NewFolder "/home/whop/hosting/whop/supervisord"
	chown -R whop:whop /home/whop
	cd /home/whop/hosting/whop/SSL
	echo -ne "\n\n\n\n\n\n\n\n" | openssl req -config $sslConfig -new -newkey rsa:2048 -days 31337 -nodes -x509 -keyout whop.key -out whop.crt
	openssl rsa -in whop.key -out whop.key

	#fix watch no space left
	echo 9999999 | sudo tee /proc/sys/fs/inotify/max_user_watches
	echo -ne "\n\nfs.inotify.max_user_watches=9999999" >> /etc/sysctl.conf


	cd $1
}

function SettingSSH {
	config="
	\nMatch Group whopjail
    \n\tAllowTcpForwarding no\n
    "
    echo -ne $config >> /etc/ssh/sshd_config

	# whop specific config
	ReplaceText "#Port 22" "Port 22" /etc/ssh/sshd_config
	ReplaceText "#UseDNS yes" "UseDNS no" /etc/ssh/sshd_config
}

function MainStartup {
	config="
	#!/bin/bash\n\n\n
	"
	echo -ne $config > /etc/mainstartup.sh
	chmod +x /etc/mainstartup.sh

	# quota check
	checkDevRoot="
if [ ! -e /dev/root ]; then
	devRoot=\`mount | grep usrquota | awk '{print \$1}'\`
	ln -s \$devRoot /dev/root
fi"

	echo -ne "$checkDevRoot" >> /etc/rc.local

	echo -ne "\nquotacheck -vagum"  >> /etc/rc.local
	echo -ne "\nquotaon /" >> /etc/rc.local
	echo -ne "\n/etc/./mainstartup.sh\n" >> /etc/rc.local
	echo -ne "\nforever start /home/whop/panel/whop-node/start.js" >> /etc/rc.local
}


function SystemUpdate {
	yum clean all
	yum update -y
}

function InstallQuota {
	yum install quota -y
	mount -o remount,usrquota /


	if [ ! -e /dev/root ]; then
		root=`mount | grep usrquota | awk '{print $1}'`
		ln -s $root /dev/root
	fi

	quotacheck -cvaumf
	quotaon /

	cd $1

	cp assets/whopsystem/whopquota /etc/init.d/whopquota
	chmod +x /etc/init.d/whopquota

	service whopquota start
	chkconfig whopquota on
}