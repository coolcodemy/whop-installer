#!/bin/bash
#
# Description: 	WHoP is a short form of Web Hosting Panel. (Read it as hop).
# Author:		Ahmad Fikrizaman Bin Abd Rahim
# Position:		Chief Technology Office at Sejuk Studio (Cool Code Sdn. Bhd.)

#####################################
# All the include script will be here
#####################################

. includes/core.sh
. includes/init.sh
. includes/install.sh
. includes/config.sh
. includes/panel.sh

colorMsg "####################################################" green
colorMsg "# Welcome to WHoP Installer. This installer" green
colorMsg "# will install application that will be used in" green
colorMsg "# WHoP. After finishing this installation," green
colorMsg "# you will have another installation which is" green
colorMsg "# WHoP configuration. Right now please" green
colorMsg "# relax and enjoy until the installation finish." green
colorMsg "####################################################" green


branch=`git rev-parse --abbrev-ref HEAD`
if [[ "$branch" == "master" ]]; then
	echo
	colorMsg "Please checkout to any tag before proceed. The download page on WHoP website will help. Exiting." red
	exit
fi

echo -ne "\n\n\n"
colorMsg "We are sending an installation information to our server." green
colorMsg "This is to make us know about our customer. Every detail" green
colorMsg "are sending via secure protocol" green
echo -ne "\n\n\n"

while true; do
    read -p "Do you wish to install WHoP? [yY] or [nN] : " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer y or n : ";;
    esac
done

echo
colorMsg "MySQL password will be generated automatically. Minimum is 12 and maximum is 60 characters" yellow
echo
read -p "1st we need to know your MySQL password length: " pwlength

while [[ ! $pwlength =~ ^[0-9]+$ || $pwlength -lt 12 || $pwlength -gt 60 ]]
do
  read -p "Password length is number only. Minimum is 12 and maximum is 60. Password length: " pwlength
done

initial=`pwd`
username="whop"
password=$(RandomStringSymbol $pwlength)
redisPassword=$(RandomString 128)
rootPassword=$(RandomStringSymbol $pwlength)

# fix for digital ocean
yum install bind-utils wget nano -y

ipaddress=`dig +short myip.opendns.com @resolver1.opendns.com`
./analytic.py


######################################################################
#
# GIT Location
#
whopVersion="1.0.2"




suhosinGit="https://github.com/stefanesser/suhosin.git"
suhosinStable="0.9.38"

whopPanelInstallerGit="https://bitbucket.org/coolcodemy/whop-panel-installer.git"
whopPanelInstallerVersion="1.0.0"

whopNodeInstallerGit="https://bitbucket.org/coolcodemy/whop-node-installer.git"
whopNodeInstallerVersion="1.0.1"

whopNodeGit="https://bitbucket.org/coolcodemy/whop-node.git"
whopNodeVersion="1.0.1"

whopPanelGit="https://bitbucket.org/coolcodemy/whop-panel.git"
whopPanelVersion="1.0.2"

#######################################################################
#
# File Location
# 
roundcubeFile="http://sourceforge.net/projects/roundcubemail/files/roundcubemail/1.1.2/roundcubemail-1.1.2-complete.tar.gz/download"
naxsiVersion="0.54rc3"
naxsi="https://github.com/nbs-system/naxsi/archive/$naxsiVersion.tar.gz"

nginxVersion="nginx-1.8.0"
nginx="http://nginx.org/download/$nginxVersion.tar.gz"

pageSpeedVersion="1.9.32.6"
pageSpeed="https://github.com/pagespeed/ngx_pagespeed/archive/release-$pageSpeedVersion-beta.zip"


######################################################################

clear
echo -ne "\n\n\n"
echo -ne "########################################\n"
colorMsg "PART 1: SeLinux, Create User, Config SSH" green
echo -ne "########################################"
echo -ne "\n\n\n"
sleep 1

DisableSeLinux
CreateUser
Init $initial
SettingSSH

MainStartup
SystemUpdate
InstallQuota $initial

clear
echo -ne "\n\n\n"
echo -ne "###############################################\n"
colorMsg "PART 2: Register WHoP, Do System Update/Upgrade" green
echo -ne "###############################################"
echo -ne "\n\n\n"
sleep 1


InstallAll $initial
InstallPHPMyAdmin $initial
InstallWebServer $initial $nginx $nginxVersion $naxsi $pageSpeed $pageSpeedVersion
InstallPostFixFTPD $initial $roundcubeFile
InstallPHPSecurity $initial $suhosinGit $suhosinStable

ConfigureMySQL $username $password
ConfigurePDNS $username $password
ConfigureNginx $initial
ConfigurePHPSecurity
ConfigurePureFTPD $username $password
ConfigurePostfix $username $password
ConfigureDovecot $username $password
ConfigureRoundcube $username $password
ConfigureSpamassassin
ConfigureCSF
ConfigureFail2Ban $initial

WHoPJail $initial $username $password
RegisterService $initial

GetInstaller $initial $username $whopPanelInstallerGit $whopPanelInstallerVersion
GetInstallerNode $initial $whopNodeInstallerGit $whopNodeInstallerVersion
GetNode $initial $whopNodeGit $whopNodeVersion
GetPanel $initial $username $password $redisPassword $ipaddress $whopPanelGit $whopPanelVersion $whopVersion
PanelNodeConfig $initial $username $password $redisPassword

ConfigureMysqlRootPassword $rootPassword

clear
echo
echo
echo "INSTALLATION SUMMARY"
echo
echo "- MySQL Root Password:" $rootPassword
echo
echo "- Username:" $username
echo "- MySQL Password:" $password
echo "- Redis Password:" $redisPassword
echo 
echo "FINISH INSTALLATION SUMMARY"
echo
echo "Now please go to https://$ipaddress:3025/ to start WHoP installation. Good luck!"
