#!/usr/bin/env python

import socket, json, pycurl, commands, os, datetime, time
from io import BytesIO

def getIP():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("gmail.com",80))
    ipAddress = s.getsockname()[0]
    s.close()
    return ipAddress

def getCountry( ipAddress ):
    url = "http://ipinfo.io/" + ipAddress
    data = BytesIO()

    c = pycurl.Curl()
    c.setopt(pycurl.URL, url)
    c.setopt(c.WRITEFUNCTION, data.write)
    c.perform()

    jsonData = json.loads(data.getvalue())
    try:
        return jsonData["country"]
    except Exception, e:
        return "Unknown"

def getProcessor():
    command = "cat /proc/cpuinfo | grep 'model name' | uniq"
    return commands.getstatusoutput(command)[1].split(':')[1].strip()

def getRam():
    mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')  # e.g. 4015976448
    return int(mem_bytes/(1024.**2))

def getDiskSpace():
    st = os.statvfs('/')
    return int(st.f_blocks * st.f_frsize / 1024.**2)

def getTime():
    return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')


ipAddress = getIP()
country = getCountry( ipAddress )
processor = getProcessor()
ram = getRam()
diskSpace = getDiskSpace()
time = getTime()

data = {}
data['ipAddress'] = ipAddress
data['country'] = country
data['processor'] = processor
data['ram'] = ram
data['diskSpace'] = diskSpace
data['installTime'] = time
postData = json.dumps(data)


c = pycurl.Curl()
c.setopt(pycurl.URL, 'https://whop.io/statistic/register')
c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
c.setopt(pycurl.POST, 1)
c.setopt(pycurl.POSTFIELDS, postData)
c.setopt(pycurl.USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)")
c.perform()