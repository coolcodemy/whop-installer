# WHoP Installer

WHoP is a short form of Web Hosting Panel. It is a panel for PHP website that serve using Nginx and PHP-FPM

To install WHoP, please make you run it in fresh installation of Centos 6.0 - 6.7 x64_86.

### Version
master

Please visit our website, [WHoP.io] for more information.

License
----
Copyright (c) 2015, Cool Code Sdn. Bhd. All rights reserved.

[WHoP.io]: https://whop.io