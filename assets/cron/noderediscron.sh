#!/bin/bash
# Written by Ahmad Fikrizaman for WHoP

configLaravel="/home/whop/panel/whop-panel/.env"
configRedis="/etc/redis.conf"
laravelDir="/home/whop/panel/whop-panel/"

cd $laravelDir

php artisan down
php artisan key:nodekey

forever stop /home/whop/panel/whop-node/start.js

newPass=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-128} | head -n 1`

oldPass=`grep "REDIS_PASSWORD" $configLaravel | cut -f2 -d =`


sed -i "s/$oldPass/$newPass/g" $configRedis
sed -i "s/$oldPass/$newPass/g" $configLaravel

/etc/init.d/redis restart
forever start /home/whop/panel/whop-node/start.js


supervisorctl restart whop-panel-master


php artisan up