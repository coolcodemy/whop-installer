#!/bin/bash
# Written by Ahmad Fikrizaman for WHoP

env="/home/whop/panel/whop-panel/.env"
license="/usr/share/whop/license/whop.license"
autoUpdate=`cat $env | grep "AUTO_UPDATE" | cut -f2 -d =`



function updater {

	rm -f /tmp/whop-update.tar.gz
	rm -rf /tmp/whop-update
	curl -s --request GET $1 --insecure > /tmp/whop-update.tar.gz

	cd /tmp

	tar -zxvf whop-update.tar.gz
	cd whop-update

	./update.sh


	cd /tmp

	rm -f /tmp/whop-update.tar.gz
	rm -rf /tmp/whop-update

	checkUpdate
}




function checkUpdate {

	myVersion=`cat $env | grep "WHOP_VERSION" | cut -f2 -d =`
	myKey=`cat $license | grep "Key:" | cut -f2 -d :`

	url="https://whop.io/checkupdate/$myKey/$myVersion"
	

	result=`curl -s --request GET $url --insecure`
	
	update=`echo $result | jq '.update'`

	if [[ "$update" == "true" ]]; then 
	
		downloadURL=`echo $result | jq .url | cut -f2 -d \"`

		updater $downloadURL
	fi
}
















if [[ "$autoUpdate" == "true" ]]; then
	checkUpdate
fi