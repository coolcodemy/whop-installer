#!/bin/bash
# Written by Ahmad Fikrizaman for WHoP


function Disabler
{
	envFile='/home/whop/panel/whop-panel/.env'

	mysqlUser=`cat $envFile | grep "DB_USERNAME=" | cut -f2 -d =`
	mysqlPassword=`cat $envFile | grep "DB_PASSWORD=" | cut -f2 -d =`
	mysqlTable=`cat $envFile | grep "DB_DATABASE=" | cut -f2 -d =`

	mysql -u$mysqlUser -p$mysqlPassword -D$mysqlTable -BNse "SELECT users.username, users.id FROM user_packages LEFT JOIN users ON user_packages.user_id = users.id WHERE users.disable=0 AND user_packages.endDate <= NOW()" | while read -r result
	do
		username=`echo "$result" | cut -f1`
		userID=`echo "$result" | cut -f2`

		echo $username

		#set user disable
		mysql -u$mysqlUser -p$mysqlPassword -D$mysqlTable -BNse "UPDATE users SET disable=1 WHERE username = '$username'"
		#disable ftp
		mysql -u$mysqlUser -p$mysqlPassword -D$mysqlTable -BNse "UPDATE ftp_users SET masterDisable=1 WHERE user_id = $userID"
		#disable mail forwarding
		mysql -u$mysqlUser -p$mysqlPassword -D$mysqlTable -BNse "UPDATE mail_forwardings SET masterDisable=1 WHERE user_id = $userID"
		#disable email
		mysql -u$mysqlUser -p$mysqlPassword -D$mysqlTable -BNse "UPDATE mail_users SET masterDisable=1 WHERE user_id = $userID"
		#shell to nologin
		/usr/bin/chsh -s /sbin/nologin $username
		#disable supervisor
		totalSupvervisor=`ls /home/whop/hosting/whop/supervisord/ | egrep "^$username-[a-zA-Z0-9]*\.conf$" | wc -l`

		if [[ $totalSupvervisor -ne 0 ]]; then

			for file in /home/whop/hosting/whop/supervisord/$username-*.conf
			do
				conf=`echo $(basename "$file" .conf)`
				supervisorctl remove $conf
			done

		fi
		#disable whoplet
		if [ -f /home/whop/hosting/$username/user.cfg ]; then

			sed -i 's/over=false/over=true/g' /home/whop/hosting/$username/user.cfg

			totalActiveWhoplet=`ls /home/whop/hosting/$username/nginx/ | egrep "^*.conf$" | wc -l`
			if [[ $totalActiveWhoplet -ne 0 ]]; then
				for config in /home/whop/hosting/$username/nginx/*\|nginx.conf
				do
					mv "$config" /home/whop/hosting/$username/nginx/"`basename $config .conf`.conf.disable"
				done
			fi
		fi
	done
}

# MAIN
Disabler
nginx -s reload