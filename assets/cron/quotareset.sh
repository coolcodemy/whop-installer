#!/bin/bash
# Written by Ahmad Fikrizaman for WHoP


function Enabler
{
	envFile='/home/whop/panel/whop-panel/.env'

	mysqlUser=`cat $envFile | grep "DB_USERNAME=" | cut -f2 -d =`
	mysqlPassword=`cat $envFile | grep "DB_PASSWORD=" | cut -f2 -d =`
	mysqlTable=`cat $envFile | grep "DB_DATABASE=" | cut -f2 -d =`

	mysql -u$mysqlUser -p$mysqlPassword -D$mysqlTable -BNse "SELECT users.username, endDate FROM user_packages LEFT JOIN users ON user_packages.user_id = users.id WHERE users.disable=0" | while read -r result
	do
		endDate=`echo "$result" | cut -f2 | sed 's/-//g'`
		now=`date +%Y%m%d`

		if [[ $endDate -ge $now ]]; then

			for file in /home/whopjail/*/var/log/nginx/traffic.log
			do
				echo '' > $file

				user=`echo $file | cut -f4 -d /`

				if [ -f /home/whop/hosting/$user/user.cfg ]; then
					sed -i 's/over=true/over=false/g' /home/whop/hosting/$user/user.cfg
					totalDisableWhoplet=`ls /home/whop/hosting/$user/nginx/ | egrep "^*.conf.disable$" | wc -l`
					if [[ $totalDisableWhoplet -ne 0 ]]; then
						for config in /home/whop/hosting/$user/nginx/*\|nginx.conf.disable
						do
							mv "$config" /home/whop/hosting/$user/nginx/"`basename $config .conf.disable`.conf"
						done
					fi
				fi
			done
		fi
	done
}

# MAIN
Enabler
nginx -s reload