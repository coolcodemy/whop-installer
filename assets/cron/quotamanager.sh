#!/bin/bash
# Written by Ahmad Fikrizaman for WHoP


function Checker
{
	for file in /home/whopjail/*/var/log/nginx/traffic.log
	do
		totalUsage=`awk '{ sum += $1 } END { print sum }' $file`

		user=`echo $file | cut -f4 -d /`

		if [ -f /home/whop/hosting/$user/user.cfg ]; then
			bandwidth=`cat /home/whop/hosting/$user/user.cfg | grep "bandwidth" | cut -f2 -d =`
			bandwidthByte=`echo "$bandwidth * 1024^3" | bc`

			if [[ $bandwidthByte -ne 0 ]]; then
				if [[ $totalUsage -ge $bandwidthByte ]]; then
					sed -i 's/over=false/over=true/g' /home/whop/hosting/$user/user.cfg
					totalActiveWhoplet=`ls /home/whop/hosting/$user/nginx/ | egrep "^*.conf$" | wc -l`
					if [[ $totalActiveWhoplet -ne 0 ]]; then
						for config in /home/whop/hosting/$user/nginx/*\|nginx.conf
						do
							mv "$config" /home/whop/hosting/$user/nginx/"`basename $config .conf`.conf.disable"
						done
					fi
				fi
			fi
		fi
	done
}

# MAIN
Checker
nginx -s reload