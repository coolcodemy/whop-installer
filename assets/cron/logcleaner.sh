#!/bin/bash
# Written by Ahmad Fikrizaman for WHoP



function clearLogRotate {

	rm -f /var/log/clamav/clamd.log-*
	rm -f /var/log/fail2ban.log-*
	rm -f /var/log/clamav/freshclam.log-*
	rm -f /var/log/lfd.log-*
	rm -f /var/log/mariadb.log-*
	rm -f /var/log/php-fpm/*.log-*
	rm -f /var/log/pureftpd.log-*
	rm -f /var/log/redis/redis.log-*
	rm -f /var/log/sa-update.log-*
	rm -f /var/log/cron-*
	rm -f /var/log/maillog-*
	rm -f /var/log/messages-*
	rm -f /var/log/secure-*
	rm -f /var/log/dracut.log-*
	rm -f /var/log/spooler-*
	rm -f /var/log/btmp-*
	rm -f /var/log/yum.log-*
	rm -f /tmp/*.log
}



function clearWhopletLog {

	totalAccessLog=`ls /home/whopjail/*/var/log/nginx/*_access.log | wc -l`
	totalErrorLog=`ls /home/whopjail/*/var/log/nginx/*_error.log | wc -l`

	if [[ $totalAccessLog -ne 0 ]]; then 
		for file in /home/whopjail/*/var/log/nginx/*_access.log
		do
			echo '' > $file
		done
	fi


	if [[ $totalErrorLog -ne 0 ]]; then 
		for file in /home/whopjail/*/var/log/nginx/*_error.log
		do
			echo '' > $file
		done
	fi

}






# main command

# remove log rotate
clearLogRotate
# clear whoplet log
clearWhopletLog